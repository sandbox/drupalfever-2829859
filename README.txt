dfrspnsv (DrupalFever Responsive Theme)

This Drupal 8 Theme is mobile friendly. In other words, it will adjust the
webpage design according to the screen size of the device displaying it. Well,
there is nothing new about it. The default Drupal 8 Themes are also Mobile
Friendly. This Theme, however, has a better configuration for image size
point-breaks as opposed to the Drupal 8 default Themes.
